package com.example.agendafirebase.objetos;

public class ReferenciasFirebase {
    final static public String URL_DATABASE = "https://agendafirebase-51039.firebaseio.com/";
    final static public String DATABASE_NAME = "agenda";
    final static public String TABLE_NAME = "contactos";
}
